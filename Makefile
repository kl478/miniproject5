rust-version:
	@echo "Rust command-line utility versions:"
	rustc --version             # Rust compiler
	cargo --version             # Rust package manager
	rustfmt --version           # Rust code formatter
	rustup --version            # Rust toolchain manager
	clippy-driver --version     # Rust linter

format:
	cargo fmt --quiet

lint:
	cargo clippy --quiet

test:
	cargo test --quiet

#### Cargo Lambda Section ####
## Watches for changes and rebuilds
watch:
	cargo lambda watch

### Build for AWS Lambda (use arm64 for AWS Graviton2)
build:
	cargo lambda build --release --arm64

deploy:
	cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::381492062366:role/cargo-lambda-role-57216d40-eb6a-40bb-a465-f7c4eec7eeaf

# deploy:
# 	cargo lambda deploy --region us-east-1

### Invoke on AWS
aws-invoke:
	cargo lambda invoke --remote mini_project5 --data-ascii '{ "student_id": "S1001"}'

run:
	cargo run

release:
	cargo build --release

all: format lint test run
