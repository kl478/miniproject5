# Mini Project 5: Serverless Rust Microservice and AWS DynamoDB

## Project Description

This project is a serverless microservice written in Rust, designed to interact with `AWS DynamoDB`. The service is designed to be scalable, reliable, and easily deployable as an AWS Lambda function, providing an API endpoint for retrieving student details based on a given student ID.

## Goals
- Create a Rust AWS Lambda function
- Implement a simple service
- Connect to a database

## DynamoDB Table Structure
The DynamoDB table used in this project is named StudentInfo. The table's schema is as follows:
- `student_id` (String): The primary key for the table, representing the unique ID of a student.
- `name` (String): The name of the student.
- `courses` (List of Strings): A list of courses the student is enrolled in.
- `major` (String): The major field of study for the student.

## Steps
1. Install Cago Lambda if haven't already.
```bash
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
``` 

2. Create a new project
```bash
cargo lambda new <Your Project Name> \
    && cd <Your Project Name>
```

3. Create a user under `AWS IAM` with the following policies:
    - `iamfullaccess`
    - `AWSLambda_FullAccess`
    - `AmazonDynamoDBFullAccess`. 
    
4. Search for `DynamoDB` on the AWS console:
    - Create a table named `StudentInfo` and set partition key (i.e. primary key). 
    - Under Explore items, create some new items as data for the service. 
    - Item example:
```json
{
  "student_id": {
    "S": "S1001"
  },
  "courses": {
    "SS": [
      "Algorithms",
      "Compilers"
    ]
  },
  "major": {
    "S": "Computer Science"
  },
  "name": {
    "S": "Jane Doe"
  }
}
```

5. `Lambda Function` Implementation:
    - Update `main.rs` with the logic to connect to `DynamoDB` and perform the required operations.
    - Ensure the table name in your code matches your DynamoDB table's name.
    - Add necessary dependencies to `Cargo.toml`.

6. Configure `Makefile`
    - Modify the invoke command to match the input parameters of your Lambda function.
    - Ensure all commands are correctly set up to build, deploy, and invoke your Lambda.


7. Running the function to test using:

```bash
make watch
```

and

```bash
make aws-invoke
```

8. Set up your access keys and region using:

```bash
aws configure
```

9. Build and deploy your Lambda function using:
```bash
make build
```

and 
```bash
make deploy
```


10. Search `Lambda function` on the AWS console. Under your Lambda function, create a trigger. 

11. Select API gateway to create a `REST API` (make sure for the method action, you select `ANY`).

12. Navigate to `Configuration` and click on the `Role name` for this Lambda function, attach the following policies to this role:
    - `AmazonDynamoDBFullAccess`
    - `AWSLambda_FullAccess`
    - `AWSLambdaBasicExecutionRole`

13. Test the functionality of the service using `Insomnia` to send `POST` requests. Fill in the following:
    - Method: `POST`
    - URL: `https://rrbszrzbf4.execute-api.us-east-1.amazonaws.com/getStudentsStage/mini_project5/studentsInfoResource`
    - JSON:
    ```json
    { "student_id": "S1001" }
    ```

14. In `Insomnia`, click `SEND` button, then we will recieve the response:

```json
{ 
  "major": "Computer Science", 
  "student_id": "S1001", 
  "student_name": "Jane Doe" 
}
```


## Deliverables
- Command results
![command](screenshots/command.png)

- DynamoDB Table
![table0](screenshots/table0.png)

![table](screenshots/table.png)

- Lambda function
![lambda](screenshots/lambda.png)

- Insomnia
![Insomnia1](screenshots/1.png)
![Insomnia2](screenshots/2.png)

