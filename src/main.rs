use aws_config::load_from_env;
use aws_sdk_dynamodb::model::AttributeValue;
use aws_sdk_dynamodb::Client;
use lambda_runtime::{service_fn, Error as LambdaError, LambdaEvent};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use std::collections::HashMap;

#[derive(Deserialize)]
struct Request {
    student_id: String, // Changed to a non-optional String for simplicity
}

#[derive(Serialize)]
struct Response {
    student_name: String,
    student_id: String,
    major: Option<String>,
}

#[tokio::main]
async fn main() -> Result<(), LambdaError> {
    let func = service_fn(handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)?;

    let config = load_from_env().await;
    let client = Client::new(&config);

    let response = search_student(&client, Some(request.student_id)).await?;
    Ok(json!(response))
}

async fn search_student(
    client: &Client,
    student_id: Option<String>,
) -> Result<(String, String, Option<String>), LambdaError> {
    let table_name = "StudentInfo";
    let mut expr_attr_values = HashMap::new();
    let mut filter_expression = String::new();

    if let Some(student_id_val) = student_id {
        // Insert the student_id into expression attribute values
        expr_attr_values.insert(
            ":student_id_val".to_string(),
            AttributeValue::S(student_id_val),
        );
        // Construct the filter expression using the student_id
        filter_expression = "student_id = :student_id_val".to_string();
    }

    // Ensure we only proceed if we have a valid student_id to filter on
    if !expr_attr_values.is_empty() {
        let result = client
            .scan()
            .table_name(table_name)
            .set_expression_attribute_values(Some(expr_attr_values))
            .filter_expression(filter_expression)
            .send()
            .await?;

        // Handle the case where no items are found
        if result.items.is_none() || result.items.as_ref().unwrap().is_empty() {
            return Err(LambdaError::from("No matching student information found"));
        }

        // Assume the first item is the one we're interested in
        let item = &result.items.unwrap()[0];

        // Extract and return the relevant fields
        let student_name = item
            .get("name")
            .and_then(|v| v.as_s().ok())
            .map(|s| s.to_string())
            .unwrap_or_default();

        let student_id = item
            .get("student_id")
            .and_then(|v| v.as_s().ok())
            .map(|s| s.to_string())
            .unwrap_or_default();

        let major = item
            .get("major")
            .and_then(|v| v.as_s().ok())
            .map(ToString::to_string);

        Ok((student_name, student_id, major))
    } else {
        // Handle the scenario where no valid student_id was provided
        Err(LambdaError::from(
            "ExpressionAttributeValues must not be empty",
        ))
    }
}
